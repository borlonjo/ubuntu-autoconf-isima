# Script de configuration de machines de prêt

Ce script permet de faire la configuration de base de d'un Ubuntu d'une machine destinée au prêt. 

Le script doit être exécuté avec la commande `sudo`
Les opérations réalisées sont les suivantes :

* Création d'un utilisateur `cri` avec privilèges *(optionnel)*
* Mise à jour du cache APT
* Remontée GLPI de la machine *(optionnel)* [https://glpi.local.isima.fr/]
* Mise à jour du système *(optionnel)*
