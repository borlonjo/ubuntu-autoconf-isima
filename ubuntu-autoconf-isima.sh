#!/bin/bash

if [[ $EUID -ne 0 ]]; then
	echo "Script must be executed with root priviledge"
	exit 1
fi

echo -n "Modifier le nom d'hôte de la machine ? (o/N) : "
read CHANGE_HOSTNAME

if [[ "$CHANGE_HOSTNAME" = "o" ]] || [[ "$CHANGE_HOSTNAME" = "O" ]]; then
    VALID_HOSTNAME=0
    while [[ $VALID_HOSTNAME -eq 0 ]]; do
            echo -n "Nom d'hôte : "
            read HOSTNAME
            if echo $HOSTNAME | egrep -q '^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$'; then
                echo $HOSTNAME > /etc/hostname
                hostname $HOSTNAME
                VALID_HOSTNAME=1
            else
                echo "Le nom d'hôte n'est pas valide."
                VALID_HOSTNAME=0
            fi
    done
fi


echo -n "Créer utilisateur 'cri' ? (o/N) : "
read CRI_USER_CREATION
if [[ "$CRI_USER_CREATION" = "o" ]] || [[ "$CRI_USER_CREATION" = "O" ]]; then
  adduser cri
  adduser cri sudo
fi
echo
echo
echo "Mise à jour de la base APT..."
apt-get update
echo
echo
NB_UPGRADABLE=$(apt list --upgradable | wc -l)
echo -n "Il y a $NB_UPGRADABLE mises à jours disponibles. Les mettres à jours ? (O/n) : "
read APT_UPGRADE
if [[ ! "$APT_UPGRADE" = "n" ]] && [[ ! "$APT_UPGRADE" = "N" ]]; then
  apt-get upgrade
fi
echo
echo
echo -n "Faire une remontée GLPI de la machine ? (O/n) : "
read GLPI_UPDATE
if [[ ! "$GLPI_UPDATE" = "n" ]] && [[ ! "$GLPI_UPDATE" = "N" ]]; then
  apt install fusioninventory-agent
  systemctl enable fusioninventory-agent
  sudo sed -ri.ori 's/^#server = .*glpi\/plugins\/fusioninventory\//server = http:\/\/glpi.local.isima.fr\/plugins\/fusioninventory\//' /etc/fusioninventory/agent.cfg
  sudo sed -ri 's/^delaytime = 3600$/delaytime = 5/' /etc/fusioninventory/agent.cfg
  sudo sed -ri.ori 's/MODE=cron/MODE=daemon/' /etc/default/fusioninventory-agent
  systemctl restart fusioninventory-agent
fi


# Installation de gnome-tweak-tool

apt install gnome-tweak-tool

# Gnome-Shell Extensions Installation

EXTENSIONS="6 7 8 15 118"

echo -n "Installer les quelques extensions GnomeShell de base [Ubuntu >=17.10 ] ? (recommandé) O/n : "
read GS_EXT
if [[ ! "$GS_EXT" = "n" ]] && [[ ! "$GS_EXT" = "N" ]]; then
    # Get distribution of Linux
    DISTRIB=$(lsb_release -d|cut -d':' -f2|sed -re 's/\t//')
    if echo $DISTRIB | grep -q Ubuntu; then
        GNOMESHELL_VERS=$(gnome-shell --version)
        echo "Le système est $DISTRIB avec $GNOMESHELL_VERS"
        echo "Installation des extensions GnomeShell"
        if [[ -x ./gnomeshell-extension-manage ]]; then
            for EXT in $EXTENSIONS; do
                ./gnomeshell-extension-manage --install --extension-id $EXT
            done
        else
            echo "File ./gnomeshell-extension-shell missing or have no execution permission"
        fi
    fi
fi
